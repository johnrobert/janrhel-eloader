/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Library;

/**
 *
 * @author jerodiaz
 */
public class Common {
    
    public String default_username = "janrhel";
    public String default_password = "password123";
    
    // Error messages
    public String login_error_message = "Username or Password is incorrect!";
    public String invalid_amount_message = "Amount must a valid number!";
    public String invalid_phone_number_message = "Invalid phone number!";
    
    // JOptionPane titles
    public String login_error_title = "LOGIN ERROR";
    public String buy_load_error_title = "BUY LOAD ERROR";
    public String buy_load_success_title = "BUY LOAD SUCCESS";
    
    // table 
    public String table_headers = "ID, PHONE #, NETWORK, AMOUNT, STATUS, DATE";
    
    // Api url
    public String get_transaction_list_url = "http://meyors.meaistore.com/java/get-transaction-list";
    public String buy_load_url = "http://meyors.meaistore.com/java/buy-load";
}
